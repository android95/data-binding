package com.example.aboutme

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.example.aboutme.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val myName: MyName = MyName("Jatuporn Hemranon")

    lateinit var myButton: Button
    lateinit var editText: EditText
    lateinit var nicknameTextView: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)



        binding.apply {
            editText = nicknameEdit
            nicknameTextView = nicknameText
            myButton = doneButton
        }

        myButton.setOnClickListener{ v ->
            addNickname(v)
        }
        nicknameTextView.setOnClickListener{v ->
           updateNickname(v)
        }
        binding.myName = myName
    }
     private fun addNickname(v: View) {

         binding.apply {
             myButton.visibility = View.GONE
             editText.visibility = View.GONE
             myName?.nickname = nicknameEdit.text.toString()
             nicknameTextView.visibility = View.VISIBLE
            invalidateAll()

             val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
             inputMethodManager.hideSoftInputFromWindow(v.windowToken, 0)

         }


    }
    private fun updateNickname (v: View) {
        editText.visibility = View.VISIBLE
        myButton.visibility = View.VISIBLE
        v.visibility = View.GONE
        editText.requestFocus()
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(v.windowToken, 0)

    }

}